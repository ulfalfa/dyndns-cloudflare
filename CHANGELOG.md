# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.5](https://gitlab.com/ulfalfa/dyndns-cloudflare/compare/0.0.4...0.0.5) (2020-01-08)

### [0.0.4](https://gitlab.com/ulfalfa/dyndns-cloudflare/compare/v0.0.3...v0.0.4) (2020-01-08)

### [0.0.3](https://gitlab.com/ulfalfa/dyndns-cloudflare/compare/v0.0.2...v0.0.3) (2020-01-08)


### Features

* allowing to update the root domain with '@' and also multiple domains ([21a4d0b](https://gitlab.com/ulfalfa/dyndns-cloudflare/commit/21a4d0b42def46a52150d2e3620e01a3d0766eda))
* dnycfhost is optional - if left out the domain entry is updated ([a674da2](https://gitlab.com/ulfalfa/dyndns-cloudflare/commit/a674da22551666dd16c29e748a8a0742e0b98129))


### Bug Fixes

* allow multiple hosts specified in env variables (s. yargs bug https://github.com/yargs/yargs/is ([bd3d01b](https://gitlab.com/ulfalfa/dyndns-cloudflare/commit/bd3d01b687fa59a58ac7d9852c87090ad3ad05c5))

### 0.0.2 (2019-12-27)


### Features

* initial commit ([523277f](https://gitlab.com/ulfalfa/dyndns-cloudflare/commit/523277fc4debe7fc1e889de8712e58fc83adaebd))
* **cloudflare:** removing cloudflare api package and replacing it by direct calls ([b22e824](https://gitlab.com/ulfalfa/dyndns-cloudflare/commit/b22e824ca44cffbc5744934b2147afbfe463ee42))
* **docker:** compose file ([09ca28b](https://gitlab.com/ulfalfa/dyndns-cloudflare/commit/09ca28b423efe7782fbb86bd40f623dd93019231))
* **docker:** listening for ctrl+c and enabled debug by default ([e72c7a6](https://gitlab.com/ulfalfa/dyndns-cloudflare/commit/e72c7a6397c9ace6fc30eae2d99f65891a7e55c0))


### Bug Fixes

* some general errors ([a9a8827](https://gitlab.com/ulfalfa/dyndns-cloudflare/commit/a9a882774fab387f3c2ff55056baa54337579120))
* some general errors ([3720fbd](https://gitlab.com/ulfalfa/dyndns-cloudflare/commit/3720fbd253548187f3fe65cf6448ac150c525cd1))
